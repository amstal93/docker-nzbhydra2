FROM alpine:3.15

LABEL org.opencontainers.image.authors = "cusoon"
LABEL org.opencontainers.image.source = "https://gitlab.com/homesrvr/docker-nzbhydra2"

RUN \
    apk update && \
    apk add -U --update --no-cache curl jq unzip=6.0-r9 ca-certificates coreutils openjdk11 tzdata bash python3 nss && \
    update-ca-certificates && \
    rm -rf /var/cache/apk/* 

RUN \
    export NZBHYDRA2_VERSION=$(curl -sX GET https://api.github.com/repos/theotherp/nzbhydra2/releases/latest | jq -r .tag_name) && \
    export NZBHYDRA2_VER=${NZBHYDRA2_VERSION#v} && \
    curl -o /tmp/nzbhydra2.zip -L "https://github.com/theotherp/nzbhydra2/releases/download/v${NZBHYDRA2_VER}/nzbhydra2-${NZBHYDRA2_VER}-linux.zip" && \
    mkdir -p /app/nzbhydra2/bin && \
    unzip /tmp/nzbhydra2.zip -d /app/nzbhydra2/bin && \
    chmod +x /app/nzbhydra2/bin/nzbhydra2wrapperPy3.py && \
    chmod +x /app/nzbhydra2/bin/nzbhydra2 && \
    rm -rf /tmp/* /var/lib/apt/lists/* /var/tmp/*

ENV PATH="/app/nzbhydra2/bin:${PATH}"
RUN mkdir -p /config/logs
COPY /startup.sh /app/nzbhydra2/bin

EXPOSE 5076
VOLUME /config /downloads

CMD ["/bin/bash", "/app/nzbhydra2/bin/startup.sh"]



