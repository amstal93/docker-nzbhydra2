#!/usr/bin/with-contenv bash

cd /app/nzbhydra2/bin || exit
echo "Starting nzbhydra2... "

exec /usr/bin/python3 nzbhydra2wrapperPy3.py --nobrowser --datafolder /config